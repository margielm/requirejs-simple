'use strict';
require.config({
    baseUrl: 'js',
    paths: {
        "jquery": "../lib/jquery/dist/jquery",
        "angular": "../lib/angularjs/angular.min",
        "domReady": "../lib/requirejs-domready/domReady"
    },
    shim: {
        "angular": {
            exports: "angular"
        }
    },
    deps: ["angular-bootstrap"]
});
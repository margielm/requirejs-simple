define(["angular", "require", "jquery", "app"], function (ng, require, $) {
    require(['domReady!'], function (document) {
        ng.bootstrap($("body"), ['myApp']);
    });
});
define(["./module"], function (module) {
    module.controller("MyController", ["$scope", function ($scope) {
        $scope.getSum = function (x, y) {
            return x + y;
        };
    }]);
});
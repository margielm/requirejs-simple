define(["angular", "controllers/index"], function (ng) {
    return ng.module("myApp", ['controllers']);
});